<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model console\models\Book */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Книги'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Alib.ru'),  $model->link->href, ['class' => 'btn btn-default','target'=> '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Распарсить'), ['parse', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Перевести'), ['/book/translate', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'link_id',
            [   'format'=> 'raw',
                'label' => 'Ссылка',
                'value' => Html::a($model->link->href,$model->link->href,['target'=>'_blank'])
            ],
            //'row_html:ntext',
            [
                'label' => 'HTML',
                'format'=> 'html',
                'value' => $model->row_html,
            ],
            'title',
            'descr',
            'addition_descr:html',
            'izdatel_city',
            'seller',
            'seller_city',
            'condition',

            [
                'label' => 'Картинки',
                'format'=> 'html',
                'value' => function($model){
                    $resultHtml= '';
                    $a = json_decode(unserialize($model->image));

                    if($a){
                        foreach ( $a as $image ){
                            $resultHtml .= '<a href="'.$image.'">'.$image.'</a>';
                        }
                    }

                    return $resultHtml;
                }
            ],
            'price:ntext',
            'year:ntext',
        ],
    ]) ?>

</div>
<style>
    td {
        max-width: 500px;
        white-space: normal !important;
        overflow: hidden;
    }
</style>