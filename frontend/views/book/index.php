<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Книги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'label' => 'Посмотреть',
                'format' => 'raw',
                'contentOptions' => ['class' => 'col-md-1','style' => 'vertical-align:middle;'],
                'value' => function ($model) {
                    if($model->link){
                        $out =  Html::a('Код ссылки','/links/view/?id='.$model->link->id,['class'=> 'btn btn-default']) .'<br>'. Html::a('Alib.ru', $model->link->href,['class'=> 'btn btn-info','style'=>'margin-top:10px','target'=>'_blank']);
                        return $out;
                    }
                }
            ],
            'row_html:html',
            //'title',
            //'descr',
            //'seller',
            //'condition',
            //'image',
            //'price:ntext',
            //'year:ntext',

            [
                'header'=>'Действия',
                'contentOptions' => ['class' => 'col-md-1','style' => 'vertical-align:middle;'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{translate}{update}{view}{delete}',
                'buttons' => [
                    'translate' => function ($url,$model) {
                            return Html::a(' Перевести', $url, ['class' => 'btn btn-warning ', 'style' => 'display:block;margin-bottom: 5px;width:120px;']);
                    },
                    'update' => function ($url,$model) {
                            return Html::a(' Редактировать', $url, ['class' => 'btn btn-primary ', 'style' => 'display:block;margin-bottom: 5px;width:120px;']);
                    },
                    'view' => function ($url,$model) {
                              return Html::a(' Просмотр', $url, ['class' => 'btn btn-info ', 'style' => 'display:block;margin-bottom: 5px;width:120px;']);
                    },
                    'delete' => function ($url,$model) {

                             return Html::a(' Удалить', $url,
                                 [
                                     'class' => 'btn btn-danger ',
                                     'data-method' => 'post',
                                     'data-confirm' => Yii::t('yii', 'Вы уверены?'),
                                     'style' => 'display:block;width:120px;'
                                 ]);

                    },

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<style>
    td {
        max-width: 500px;
        white-space: normal !important;
        overflow: hidden;
    }
    .centred{
        vertical-align:middle;
    }
</style>