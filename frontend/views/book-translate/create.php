<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model console\models\BookTranslate */

$this->title = Yii::t('app', 'Create Book Translate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Book Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-translate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
