<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model console\models\BookTranslateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-translate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'link_id') ?>

    <?= $form->field($model, 'row_html') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'descr') ?>

    <?php // echo $form->field($model, 'seller') ?>

    <?php // echo $form->field($model, 'condition') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'translate') ?>

    <?php // echo $form->field($model, 'seller_city') ?>

    <?php // echo $form->field($model, 'addition_descr') ?>

    <?php // echo $form->field($model, 'izdatel_city') ?>

    <?php // echo $form->field($model, 'xml_file_name') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
