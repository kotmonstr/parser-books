<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model console\models\BookTranslate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-translate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'link_id')->textInput() ?>

    <?= $form->field($model, 'row_html')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seller')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'condition')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'year')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'translate')->textInput() ?>

    <?= $form->field($model, 'seller_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'addition_descr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'izdatel_city')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'xml_file_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
