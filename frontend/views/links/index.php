<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ссылки на книги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'href:text',
            [
                'label' => 'Книга',
                'format' => 'html',
                 'value' => function ($model) {
                    if($model->book){
                        return \yii\helpers\Html::a('Просмотреть','/book/view/?id='.$model->book->id,['class'=> 'btn btn-primary']);
                    }else{
                        return "<span class='alert-danger'>Идет работа парсера...</span>";
                    }
                 }
            ],      [
                'label' => 'На сайте Alib.ru',
                'format' => 'html',
                 'value' => function ($model) {
                     return \yii\helpers\Html::a('Перейти',$model->href,['target'=>'_blank','class'=> 'btn btn-info']);
                 }
            ],

            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => [ 0 => 'Не обработана', 1 => 'Обработана'],
                'value' => function ($model, $key, $index, $column) {
                    $active = $model->{$column->attribute} === 1;
                    return \yii\helpers\Html::tag(
                        'span',
                        $active ? 'Обработана' : 'Не обработана',
                        [
                            'class' => 'label label-' . ($active ? 'success' : 'danger'),
                        ]
                    );
                },
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['datetime', 'php:Y-m-d h:i:s'],
            ],
            [
                'header'=>'Действия',
                'contentOptions' => ['class' => 'col-md-1','style' => 'vertical-align:middle;'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{view}{delete}',
                'buttons' => [
                    'update' => function ($url,$model) {

                        return Html::a(' Редактировать', $url, ['class' => 'btn btn-primary ', 'style' => 'display:block;margin-bottom: 5px;width:120px;']);

                    },
                    'view' => function ($url,$model) {

                        return Html::a(' Просмотр', $url, ['class' => 'btn btn-info ', 'style' => 'display:block;margin-bottom: 5px;width:120px;']);

                    },
                    'delete' => function ($url,$model) {

                        return Html::a(' Удалить', $url,
                            [
                                'class' => 'btn btn-danger ',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('yii', 'Вы уверены?'),
                                'style' => 'display:block;width:120px;'
                            ]);

                    },

                ],
            ],
        ],
    ]); ?>
</div>
<style>
    td {
        max-width: 500px;
        //white-space: normal !important;
        overflow: hidden;
    }
</style>