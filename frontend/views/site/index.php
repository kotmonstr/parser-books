<?php

/* @var $this yii\web\View */

$this->title = 'Parser';
?>
<div class="site-index">

        <h3>Статистика работы парсера</h3>
                        <p>
                         #1. Cбор ссылок - реализовано.<br>
                         #2. Перевод- запущен.<br>
                         #3. Сбор книг- реализовано.<br>
                         #3. Оптимизация парсинга полей - в процессе.<br>
                         #4. Оптимизация скорости - реализовано<br>
                         #5. Мульти запросы для поиска урл - реализовано<br>
                         #6. Мульти запросы для оброботки книг - реализовано</br>
                         #7. Импорт - в процессе</br>
                         #8. Cбор ссылок вживую - в процессе.<br>
                        </p>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h3>Ссылок  <b><?= Yii::$app->formatter->asDecimal($linksCount) ?></b></h3>
            </div>
            <div class="col-lg-6">
                <h3>Книг <b><?= Yii::$app->formatter->asDecimal($bookCount) ?></b> </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h5><?= '  Готово: <b>'.Yii::$app->formatter->asDecimal($linksNoneFinishedCount).' ('. $linkInterest .')</b>' ?><?= '<br>  Осталось: <b>' .Yii::$app->formatter->asDecimal($linksFinishedCount).'</b>' ?></h5>
            </div>
            <div class="col-lg-6">
                <h5><?= 'Переведено и распарсено: <b>'.Yii::$app->formatter->asDecimal($modelBookTranslateCount).'</b>' ?><?= '<br>  Осталось: <b>' .Yii::$app->formatter->asDecimal($modelBookNonTranslateCount).'</b>' ?></h5>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h5>Скачать дамп бд: </h5>

<?php

    foreach ($sqlFiles as $file):

        $nameFicheiro = substr($file, strrpos($file, '/') + 1);
        echo \yii\helpers\Html::a($nameFicheiro, '/dumps/'.$nameFicheiro).'<br>';

    endforeach;

?>

            </div>
        </div>



        <?php if(!empty($arrFiles)): ?>
            <div class="row" style="margin-top: 20px">
            <div class="col-lg-12">
                <table class="table table-bordered table-condensed table-responsive ">
                    <thead>
                        <tr>
                            <td>Имя файла</td>
                            <td>Страницы</td>
                            <td>Найдено уникальных ссылок</td>
                            <td>Книги</td>
                        </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($arrFiles as $file): ?>

                   <tr>
                        <td><?php echo $file['name'] ?></td>
                        <td><?php echo $file['page'] ?></td>
                        <td><?php echo $file['link'] ?></td>
                        <td><?php echo $file['book'] ?></td>
                    </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <?php endif; ?>

    </div>
</div>
