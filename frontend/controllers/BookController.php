<?php

namespace frontend\controllers;

use console\models\BookTranslate;
use console\models\City;
use Yii;
use console\models\Book;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Sunra\PhpSimple\HtmlDomParser;
use console\models\BookSearch;
use yii\helpers\ArrayHelper;
/**
 * GrudController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    const KEY = "trnsl.1.1.20190304T092450Z.dfe2c2c37b0cb874.6f797bed6a5e208205a59f1adcca18cfd40ee43c"; // testolegkot
    const LANGUAGE = "ru-en";

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

//        $dataProvider = new ActiveDataProvider([
//            'query' => Book::find(),
//        ]);

        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionParse($id){

        $model = $this->findModel($id);
        $source = $model->row_html; // источник
        $model->title = $this->getTitle($source);
        $model->price = $this->getPrice($source);
        $model->year = $this->getYear($source);
        $model->condition = $this->getCondition($source);
        $model->seller = $this->getSeller($source);
        $model->seller_city = $this->getSellerCity($source);
        $model->descr = $this->getDescription($source);
        $model->image = serialize(json_encode($this->getImage($source)));
        $model->addition_descr =$this->getAdditionDescr($source);
        $model->izdatel_city =$this->getIzdatelCity($source);
        $model->translate = Book::NON_TRANSALATED;
        $model->updateAttributes(['title','price','year','condition','seller','seller_city','descr','image','addition_descr','translate','izdatel_city']);

        return $this->redirect('view?id='.$id);
    }

    private function getTitle($source){

        $regexp = '~<b>.+\</b>~';

        $match = [];
        if (preg_match($regexp, $source, $match)) {
            //$x= "+ Найдено слово '{$match[0]}'\n";
            //vd($match,false);
            $x = $match[0];
        } else {
            $x= "";
        }

    return trim(strip_tags($x));
    }
    private function getPrice($source){

        $regexp = '~Цена:.+?руб.~m';

        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }

    return trim(str_replace("руб.","",str_replace("Цена:","",$x)));
    }
    private function getYear($source){
        $regexp = '~[0-9]{4}г\.~';
        $regexp2 = '~[0-9]{4}\s+г\.~';
        $regexp3 = '~[0-9]{4}\(~';
        $match = [];
        if (preg_match($regexp, $source, $match) || preg_match($regexp2, $source, $match) || preg_match($regexp3, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return trim(str_replace("(","",str_replace("г.","",$x)));
    }
    private function getCondition($source){
        $regexp = '~Состояние:.+?\<~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace("<","",str_replace("Состояние:","",$x)));
    }
    private function getSeller($source){

        $regexp = '~BS.+?<~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }

        return  urldecode(trim(str_replace("BS -","",str_replace("<","",preg_replace("~BS.+?>~","",$x)))));
    }
    private function getSellerCity($source){
        $regexp = '~a\>,.+?\)~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace(".)","",str_replace("a>,","",$x)));
    }
    private function getDescription($source){
        $regexp = '~/b>.+?[0-9]{2,}.+?с\.~';
        $regexp2 = '~/b>.+?<br~';
        $match = [];
        if (preg_match($regexp, $source, $match) || preg_match($regexp2, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace("<br","",str_replace("/b>","",$x)));
    }
    private function getImage($source){
        $i=0;
        $result= [];
        $source = str_replace("amp;","",$source);
        $regexp = '~http:\/\/www.alib.ru\/foto.php4\?.+?\"~';
        $match = [];
        if (preg_match_all($regexp, $source, $match)) {
            $x = $match;
        } else {
            $x=[];
        }
        if(!empty($x[0])){
            foreach ($x[0] as $row => $item){
                $i++;
                $result[$i] = trim(str_replace("\"","",$item));
            }
        }


        return  $result;
    }
    private function getAdditionDescr($source){

        //vd($source,false);
        $regexp = '~\sс\..+?Состояние~';
        $regexp2 = '~[0-9]{2,}с\..+?Состояние~';
        $regexp3 = '~[0-9]{2,}с.+?Состояние~';
        $regexp4 = '~[0-9]{2,}.c\..+?Состояние~';

        $match = [];
        if ( preg_match($regexp, $source, $match ) || preg_match($regexp2, $source, $match) ||  preg_match($regexp3, $source, $match) || preg_match($regexp4, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  '<p>'.trim(preg_replace("~^\.~","",preg_replace("~с\.~","",preg_replace("~[0-9]{1,}.c\.~","",preg_replace("~[0-9]{1,}.c\.~","",preg_replace("~[0-9]{1,}с~","",str_replace("Состояние","",$x))))))).'</p>';



        // мой первый вариант отлично работает
        //*****************************************************************
        //        $pieces =  explode('<br>',$source);
        //        $regexp = '~Состояние~';
        //        $match = [];
        //        if (preg_match($regexp, $pieces[2], $match)) {
        //            $x = $match[0];
        //        } else {
        //            $x= "";
        //        }
        //        return  $pieces[2] &&  empty($x) ? trim($pieces[2]) : false;
        // *************************************************************************
    }


    public function actionTranslate($id){

        $model = $this->findModel($id);

        if( $model->translate == Book::TRANSLATED){
            return $this->redirect(['book-translate/view/?id='.$model->translated->id]);
        }

        $source = $model->row_html; // источник
        $model->title = $this->getTitle($source);
        $model->price = $this->getPrice($source);
        $model->year = $this->getYear($source);
        $model->condition = $this->getCondition($source);
        $model->seller = $this->getSeller($source);
        $model->seller_city = $this->getSellerCity($source);
        $model->descr = $this->getDescription($source);
        $model->image = serialize(json_encode($this->getImage($source)));
        $model->addition_descr =$this->getAdditionDescr($source);
        $model->updateAttributes(['title','price','year','condition','seller','seller_city','descr','image','addition_descr']);

        $modelTranslate = new BookTranslate();
        $modelTranslate->link_id = $model->link_id;
        $modelTranslate->book_id = $model->id;
        $modelTranslate->row_html =  Html::decode($this->T($model->row_html));
        $modelTranslate->title =  Html::decode($this->T($model->title));
        $modelTranslate->descr =  Html::decode($this->T($model->descr));
        $modelTranslate->seller =  $model->seller;
        $modelTranslate->condition =  Html::decode($this->T($model->condition));
        $modelTranslate->image =  $model->image;
        $modelTranslate->price =  $model->price;
        $modelTranslate->year =  $model->year;
        $modelTranslate->translate = 1;
        $modelTranslate->seller_city =  $model->seller_city ?  Html::decode($this->T($model->seller_city)) : null;
        $modelTranslate->addition_descr =  $model->addition_descr ?  str_replace("</ p>","</p>",Html::decode($this->T($model->addition_descr))) : null;
        $modelTranslate->izdatel_city =  $model->izdatel_city ?   Html::decode($this->T($model->izdatel_city)) : null;
        $modelTranslate->xml_file_name = $model->xml_file_name;

        if( $modelTranslate->validate() &&  $modelTranslate->save() ){
            $model->translate = Book::TRANSLATED;
            $model->translate_id = $modelTranslate->id;
            $model->updateAttributes(['translate','translate_id']);
            return $this->redirect(['book-translate/view/?id='.$modelTranslate->id]);
        }else{
                vd($modelTranslate->getErrors());
                Yii::$app->session->setFlash('error', 'ERROR FOR TRANSLATE.');
                return $this->redirect(['book/view/?id='.$modelTranslate->id]);
        }

    }

//    private function T($text){
//        $url = "https://translate.yandex.net/api/v1.5/tr/translate?key=" . self::KEY . "&lang=" . self::LANGUAGE . "&text=" . $text;
//        $result = simplexml_load_file($url);
//        return $result->text;
//    }

    private function T($text){

        //http://chuvyr.ru/2014/01/gtranslate.html

        $lang_from = "ru";
        $lang_to = "en";

        $query_data = array(
            'client' => 'x',
            'q' => $text,
            'sl' => $lang_from,
            'tl' => $lang_to
        );
        $filename = 'http://translate.google.ru/translate_a/t';
        $options = array(
            'http' => array(
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.0; rv:26.0) Gecko/20100101 Firefox/26.0',
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($query_data)
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($filename, false, $context);
        return  json_decode($response);
    }


    public function strposX($haystack, $needle, $number)
    {
        // decode utf8 because of this behaviour: https://bugs.php.net/bug.php?id=37391
        preg_match_all("/$needle/", utf8_decode($haystack), $matches, PREG_OFFSET_CAPTURE);
        return $matches[0][$number-1][1];
    }


    private function getIzdatelCity($source){


        $regexp = '~/b>.+?<br~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        $source = trim($x);


        //************************ cityes ****************************************************************
        $arr = file(Yii::getAlias('@console/models/city.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($arr as $row){
            $arrCity1[]['city'] = $row;
        }
        $arrCity2 = City::getlistCountriesByRussian();
        $arrayTotal= ArrayHelper::merge($arrCity1, $arrCity2);
       // vd($arrayTotal);
        //*************************************************************************************

        vd($source.PHP_EOL,false);
        //обрезка фамилий с инициалами
        $regexpFIO = '~(\w+)\s[А-ЯA-Z]{1}\.\s[А-ЯA-Z]{1}\.\s~u';
        $source = preg_replace($regexpFIO,"",$source);

        $regexpFIO2 = '~\s[А-ЯA-Z]{3,}\s[А-ЯA-Z]{3}\.\s[А-ЯA-Z]{3}\.\s~u';
        $source = preg_replace($regexpFIO2,"",$source);

        //vd($source);

        foreach ($arrayTotal as $item){
            //vd('Ищу по городу: '.$item["city"].PHP_EOL,false);
            $regexp = '~\s'.$item["city"].'\s~';
            $regexp2 = '~\s'.$item["city"].',\s~';
            $regexp3 = '~\s'.$item["city"].':~';
            $regexp4 = '~\sг\.'.$item["city"].':~';
            $regexp5 = '~\s'.$item["city"].'\.\s~';

            //vd($regexp,false);

            $match = [];
            if (
                   preg_match($regexp, $source, $match)
                || preg_match($regexp2, $source, $match)
                || preg_match($regexp3, $source, $match)
                || preg_match($regexp4, $source, $match)
                || preg_match($regexp5, $source, $match)
               ){

                $x = $match[0];

                //vd($source,false);
               // vd($item["city"]);
                // ичключения ************************************
                $item["city"] = $this->FixExseption($item["city"]);

                //vd( trim($item["city"]));
                $x = trim($item["city"]);
                break;
            } else {
                $x= null;
            }
        }

        //vd(trim($x));
        return  trim($x);
    }
    private function FixExseption($city){
        //vd($city,false);
        $result = $city;
        if($city == 'М'){$result = 'Москва'; }
        if($city == 'М.'){$result = 'Москва'; }
        if($city == 'Спб.'){$result = 'Санкт-Петербург'; }
        if($city == 'Л.'){$result = 'Ленинград'; }
        if($city == 'Лен'){$result = 'Ленинград'; }
        if($city == 'Ростов-н-Д'){$result = 'Ростов-на-Дону'; }
        if($city == 'Йорк'){$result = 'Нью-Йорк'; }
        if($city == 'СПб.'){$result = 'Санкт-Петербург'; }
        if($city == 'Спб'){$result = 'Санкт-Петербург'; }
        if($city == 'М.-Л.'){$result = 'Москва - Ленинград'; }

        return $result;
    }
}
