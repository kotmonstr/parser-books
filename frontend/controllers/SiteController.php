<?php
namespace frontend\controllers;

use console\models\Book;
use console\models\Links;
use console\models\Url;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\FileHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        //vd(Yii::getAlias('@dumps'));
        $sqlFiles = FileHelper::findFiles(Yii::getAlias('@dumps'),['only'=>['*.sql']]);
        //vd($sqlFiles);
       // $arrFiles = $this->getFilesArr();

        $urlCount = Url::find()->count();
        $urlFinishedCount = Url::find()->active()->count();
        $urlNoneFinishedCount = Url::find()->noneActive()->count();
        $urlInterest = Yii::$app->formatter->asPercent(($urlNoneFinishedCount)/$urlCount,2);

        $linksCount = Links::find()->count(); //1994
        $linksFinishedCount = Links::find()->active()->count();
        $linksNoneFinishedCount = Links::find()->noneActive()->count(); //1956
        $linkInterest = Yii::$app->formatter->asPercent(($linksNoneFinishedCount)/$linksCount,2);

        $bookCount = Book::find()->count();

        $modelBookNonTranslateCount = Book::find()->select('id')->where(['translate'=> Book::NON_TRANSALATED])->asArray()->count();
        $modelBookTranslateCount = Book::find()->select('id')->where(['translate'=> Book::TRANSLATED])->asArray()->count();

        return $this->render('index',[
            'urlCount'=> $urlCount,
            'urlFinishedCount'=> $urlFinishedCount,
            'urlNoneFinishedCount'=> $urlNoneFinishedCount,
            'urlInterest'=> $urlInterest,
            'linksCount'=> $linksCount,
            'linksNoneFinishedCount'=> $linksNoneFinishedCount,
            'linksFinishedCount'=> $linksFinishedCount,
            'linkInterest' => $linkInterest,
            'bookCount'=> $bookCount,
            //'arrFiles'=> $arrFiles,
            'modelBookNonTranslateCount'=> $modelBookNonTranslateCount,
            'modelBookTranslateCount'=> $modelBookTranslateCount,
            'sqlFiles'=> $sqlFiles
            ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    public function getFilesArr(){
        $arr = [];
        return $arr;
        $ii=1;
        for ($i = 65; $i <= 70; $i++) {
            $fileName = 'tsiteau'.$i.'.xml';

//            $file = simplexml_load_file(Yii::getAlias('@dist/xml/'.$fileName));
//
//            foreach ($file as $block) {
//                $ii++;
//                $arrUrlList[$ii] = (string)$block->loc;
//            }

            //$file->count();
            $arr[$i]['name'] = $fileName;
            $arr[$i]['page'] = 1500;//count($arrUrlList);//1500
            $arr[$i]['url'] = 1499;//Url::find()->byXmlFileName($fileName)->count();//1499
            $arr[$i]['link'] = Links::find()->byXmlFileName($fileName)->count();
            $arr[$i]['book'] = Book::find()->byXmlFileName($fileName)->count();
            $arrUrlList = [];

        }
        $fileName = 'tsiteau70.'.'xml';
        $arr[70]['name'] = $fileName;
        $arr[70]['page'] = 1500; //count($arrUrlList);
        $arr[70]['url'] = 1499;//Url::find()->byXmlFileName($fileName)->count();
        $arr[70]['link'] = Links::find()->byXmlFileName($fileName)->count();
        $arr[70]['book'] = Book::find()->byXmlFileName($fileName)->count();
        //vd($arr);
        return $arr;
    }

}
