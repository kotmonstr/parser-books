<?php

namespace console\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pagination".
 *
 * @property int $id
 * @property string $url
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Pagination extends \yii\db\ActiveRecord
{

    const NONE_RESOLVED = 0;
    const RESOLVED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pagination';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['url'], 'required'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return PaginationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaginationQuery(get_called_class());
    }

    public static function addToTable($url){


        $model = new self();
        $model->url = $url;
        $model->status = '0';

        if($model->save()){
            echo "SUCCESS "." WAS ADDED id= ". $model->id. PHP_EOL;
        }else{
            $model->validate();
            echo "ERROR". PHP_EOL;
            var_dump( $model->getErrors());

        }
    }

    public static function resolveLink($id){

        $model = self::find()->where(['id'=> $id])->one();
        $model->status = self::RESOLVED;
        $model->updateAttributes(['status']);
        echo "Готов Page id=" . $id . PHP_EOL;
    }
}
