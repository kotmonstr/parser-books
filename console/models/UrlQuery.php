<?php

namespace console\models;

/**
 * This is the ActiveQuery class for [[Url]].
 *
 * @see Url
 */
class UrlQuery extends \yii\db\ActiveQuery
{

    const NONE_RESOLVED = 0;
    const RESOLVED = 1;

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Url[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Url|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active()
    {
        return $this->andWhere(['status'=> self::NONE_RESOLVED]); //0
    }

    public function noneActive()
    {
        return $this->andWhere(['status'=> self::RESOLVED]); //1
    }

    public function byXmlFileName($xmlFileName)
    {
        return $this->andWhere(['xml_file_name'=> $xmlFileName]);
    }



}
