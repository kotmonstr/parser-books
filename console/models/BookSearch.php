<?php

namespace console\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use console\models\Book;

/**
 * BookSearch represents the model behind the search form of `console\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'link_id', 'translate', 'translate_id'], 'integer'],
            [['row_html', 'title', 'descr', 'seller', 'condition', 'image', 'price', 'year', 'xml_file_name', 'seller_city', 'addition_descr', 'izdatel_city'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'link_id' => $this->link_id,
            'translate' => $this->translate,
            'translate_id' => $this->translate_id,
        ]);

        $query->andFilterWhere(['like', 'row_html', $this->row_html])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descr', $this->descr])
            ->andFilterWhere(['like', 'seller', $this->seller])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'year', $this->year])
            ->andFilterWhere(['like', 'xml_file_name', $this->xml_file_name])
            ->andFilterWhere(['like', 'seller_city', $this->seller_city])
            ->andFilterWhere(['like', 'addition_descr', $this->addition_descr])
            ->andFilterWhere(['like', 'izdatel_city', $this->izdatel_city]);

        return $dataProvider;
    }
}
