<?php

namespace console\models;

/**
 * This is the ActiveQuery class for [[BookTranslate]].
 *
 * @see BookTranslate
 */
class BookTranslateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BookTranslate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BookTranslate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byXmlFileName($xmlFileName)
    {
        return $this->andWhere(['xml_file_name'=> $xmlFileName]);
    }
}
