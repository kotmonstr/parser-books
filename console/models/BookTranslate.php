<?php

namespace console\models;

use Yii;
use console\models\Links;
use console\models\Book;

/**
 * This is the model class for table "book_translate".
 *
 * @property int $id
 * @property int $link_id
 * @property string $row_html
 * @property string $title
 * @property string $descr
 * @property string $seller
 * @property string $condition
 * @property string $image
 * @property string $price
 * @property string $year
 * @property int $translate
 * @property string $seller_city
 * @property string $addition_descr
 * @property string $izdatel_city
 * @property string $xml_file_name
 *
 * @property Links $link
 */
class BookTranslate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_translate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'translate','book_id'], 'integer'],
            [['image', 'price', 'year', 'addition_descr', 'izdatel_city','row_html'], 'string'],
            [['title', 'descr', 'seller', 'condition', 'seller_city', 'xml_file_name'], 'string'],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => Links::className(), 'targetAttribute' => ['link_id' => 'id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_id' => Yii::t('app', 'Ссылка'),
            'row_html' => Yii::t('app', 'Сырой HTML'),
            'title' => Yii::t('app', 'Наименование'),
            'descr' => Yii::t('app', 'Описание'),
            'seller' => Yii::t('app', 'Продавец'),
            'seller_city' => Yii::t('app', 'Город продавца'),
            'condition' => Yii::t('app', 'Состояние'),
            'image' => Yii::t('app', 'Картинка'),
            'price' => Yii::t('app', 'Цена'),
            'year' => Yii::t('app', 'Год'),
            'xml_file_name' => Yii::t('app', 'Имя Файла XML'),
            'izdatel_city' => Yii::t('app', 'Город издательства'),
            'addition_descr' => Yii::t('app', 'Дополнение'),
        ];


    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Links::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Links::className(), ['id' => 'book_id']);
    }

    /**
     * {@inheritdoc}
     * @return BookTranslateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookTranslateQuery(get_called_class());
    }
}
