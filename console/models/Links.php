<?php

namespace console\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use console\models\Book;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "links".
 *
 * @property int $id
 * @property string $href
 * @property int $status
 * @property string $xml_file_name
 * @property string $slug
 * @property int $created_at
 * @property int $updated_at
 */
class Links extends \yii\db\ActiveRecord
{
    const NONE_RESOLVED = 0;
    const RESOLVED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => null,
                'immutable' => false,//неизменный
               // 'ensureUnique'=>true,//генерировать уникальный
                'slugAttribute' => 'slug',
                'value' => function ( $event) {
                    return md5($this->href);
                }
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['href'], 'required'],
            ['slug', 'safe'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['href', 'xml_file_name','slug'], 'string' ],
            [['href'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'href' => Yii::t('app', 'Ссылка'),
            'status' => Yii::t('app', 'Статус'),
            'xml_file_name' => Yii::t('app', 'Имя Файла XML'),
            'created_at' => Yii::t('app', 'Созданно'),
            'updated_at' => Yii::t('app', 'Обновленно'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LinksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LinksQuery(get_called_class());
    }

    public static function addToTable($href,$xmlFileName){


            $model = new self();
            $model->href = $href;
            $model->status = '0';
            $model->xml_file_name = $xmlFileName;

            if($model->save()){
                echo "SUCCESS "." WAS ADDED id= ". $model->id. PHP_EOL;
            }else{
                $model->validate();
                echo "ERROR". PHP_EOL;
                var_dump( $model->getErrors());

            }
    }

    public static function checkUniqHref($slug){
        $model= self::find()->where(['slug'=> $slug])->one();
        if(!$model){
            echo "Найдена новая ссылка на книгу".PHP_EOL;
            return true;
        }else{
            echo "Повтор ссылки на книгу".PHP_EOL;
            return false;
        }
    }

    public static function resolveLink($id){

        $model = self::find()->where(['id'=> $id])->one();
        $model->status = self::RESOLVED;
        $model->updateAttributes(['status']);
        echo "Готов Links id=" . $id . PHP_EOL;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['link_id' => 'id']);
    }

}
