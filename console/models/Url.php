<?php

namespace console\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "url".
 *
 * @property int $id
 * @property string $url
 * @property int $status
 * @property string $xml_file_name
 * @property int $created_at
 * @property int $updated_at
 */
class Url extends \yii\db\ActiveRecord
{
    const NONE_RESOLVED = 0;
    const RESOLVED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
               // 'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => null,
                'immutable' => false,//неизменный
                // 'ensureUnique'=>true,//генерировать уникальный
                'slugAttribute' => 'slug',
                'value' => function ( $event) {
                    return md5($this->url);
                }
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['url','xml_file_name','slug'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'status' => Yii::t('app', 'Status'),
            'xml_file_name' => Yii::t('app', 'Имя Файла XML'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return UrlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UrlQuery(get_called_class());
    }

    /**
     * @param $arr
     */
    public static function AddUrlToTable($url,$xmlFileName){
        //vd($xmlFileName);
        $model = new self();
        $model->url = $url;
        $model->status = self::NONE_RESOLVED;
        $model->xml_file_name = $xmlFileName;

        if($model->save()){
            echo "SUCCESS "." WAS ADDED id= ". $model->id. PHP_EOL;
        }else{
            $model->validate();
            $model->getErrors();
            vd($model->getErrors());
            echo "ERROR". PHP_EOL;
        }


    }

    public static function resolveUrl($id){
        $model = self::find()->where(['id'=> $id])->one();
        $model->status = self::RESOLVED;
        $model->updateAttributes(['status']);
        echo "Страница с Id=".$id. " обработанна. ". PHP_EOL;
    }

    public static function checkUniqUrl($slug){
        $model= self::find()->where(['slug'=> $slug])->one();
        if(!$model){
            return true;
        }else{
            return false;
        }
    }


}
