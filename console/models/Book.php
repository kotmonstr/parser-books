<?php

namespace console\models;

use Yii;
use console\models\Links;
use console\models\BookTranslate;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property int $link_id
 * @property string $row_html
 * @property string $title
 * @property string $descr
 * @property string $addition_descr
 * @property string $seller
 * @property string $seller_city
 * @property string $condition
 * @property string $image
 * @property string $price
 * @property string $year
 * @property string $xml_file_name
 *
 * @property Links $link
 */
class Book extends \yii\db\ActiveRecord
{
    const NON_TRANSALATED = 0;
    const TRANSLATED = 1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id','translate_id'], 'integer'],
            [['row_html', 'link_id' ], 'required'],
           // [['row_html', 'title', 'descr', 'seller', 'condition', 'image', 'price', 'year'], 'required'],
            [['row_html', 'price', 'year', 'xml_file_name','seller_city','addition_descr','izdatel_city'], 'string'],
            [['title', 'descr', 'seller', 'condition', 'image'], 'string'],
            [['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => Links::className(), 'targetAttribute' => ['link_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_id' => Yii::t('app', 'Ссылка'),
            'row_html' => Yii::t('app', 'Сырой HTML'),
            'title' => Yii::t('app', 'Наименование'),
            'descr' => Yii::t('app', 'Описание'),
            'seller' => Yii::t('app', 'Продавец'),
            'seller_city' => Yii::t('app', 'Город продавца'),
            'condition' => Yii::t('app', 'Состояние'),
            'image' => Yii::t('app', 'Картинка'),
            'price' => Yii::t('app', 'Цена'),
            'year' => Yii::t('app', 'Год'),
            'xml_file_name' => Yii::t('app', 'Имя Файла XML'),
            'izdatel_city' => Yii::t('app', 'Город издательства'),
            'addition_descr' => Yii::t('app', 'Дополнение'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Links::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslated()
    {
        return $this->hasOne(BookTranslate::className(), ['id'=> 'translate_id']);
    }

    /**
     * {@inheritdoc}
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    public static function addRowHtml($text,$id,$xmlFileName){
        //vd($url,false);
        $model = new self();
        $model->link_id = $id;
        $model->row_html = $text;
        $model->xml_file_name = $xmlFileName;

        if($model->save()){
            echo "SUCCESS "." WAS ADDED id= ". $model->id. PHP_EOL;
        }else{
            $model->validate();
            var_dump( $model->getErrors());
            echo "ERROR". PHP_EOL;
        }
    }

    public static function checkUniqContent($link_id){
        $model= self::find()->where(['link_id'=> $link_id])->one();
        if(!empty($model)){
            return false;
        }else{
            return true;
        }
    }

    public static function checkIsItAdvert($text){

        $pos = strripos($text,'Место для рекламы');
        $pos2 = strripos($text,'Бланк заказа');
        if ($pos == false && $pos2 == false) {
            echo "Это книга ".PHP_EOL;
            return false;
        } else {
            echo "К сожалению это реклама".PHP_EOL;
            return true;
        }
    }

    public static function checkIsItMetrika($text){

        $pos = strripos($text,'LiveInternet counter');
        if ($pos == false) {
            echo "Это книга ".PHP_EOL;
            return false;
        } else {
            echo "К сожалению это метрика".PHP_EOL;
            return true;
        }
    }


}
