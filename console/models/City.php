<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $city
 * @property string $telcode
 * @property int $timezone
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['timezone'], 'integer'],
            [['city'], 'string', 'max' => 255],
            [['telcode'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city' => Yii::t('app', 'City'),
            'telcode' => Yii::t('app', 'Telcode'),
            'timezone' => Yii::t('app', 'Timezone'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }

    public static function getlistCountriesByRussian(){
        return self::find()->select('city')->asArray()->all();
    }
}
