<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201451_crate_book_traslate_table_and_alter_book_table extends Migration
{

    protected $tableName1 = '{{%book}}';
    protected $tableName2 = '{{%book_translate}}';

    public function up()
    {
        $this->addColumn( $this->tableName1, 'translate', $this->tinyInteger(4)->defaultValue(0));
        $this->addColumn( $this->tableName1, 'translate_id', Schema::TYPE_INTEGER);

        $this->createTable($this->tableName2, [
            'id' => Schema::TYPE_PK,
            'link_id' => Schema::TYPE_INTEGER,
            'book_id' => Schema::TYPE_INTEGER,
            'row_html' => $this->text()->notNull(),
            'title' => $this->string(255),
            'descr' => $this->string(255),
            'seller' => $this->string(255),
            'condition' => $this->string(255),
            'image' => $this->text(),
            'price' =>  $this->text(),
            'year' => $this->text(),
            'translate' => $this->tinyInteger(4)->defaultValue(0),
            'seller_city' => $this->string(255),
            'addition_descr' => $this->text(),
            'izdatel_city' => $this->text(),
            'xml_file_name' => $this->string(255),

        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        // indexes
        $this->createIndex('link-id-translate', $this->tableName2, 'link_id');
        $this->createIndex('book-id-translate', $this->tableName2, 'book_id');

        $this->addForeignKey(
            'fk-links-id-translate',//name
            'book_translate', //current table
            'link_id',//current my field
            'links', //
            'id'
        );

        $this->addForeignKey(
            'fk-book-id-translate',//name
            'book_translate', //current table
            'book_id',//current my field
            'book', //
            'id'
        );



    }

    public function down()
    {
        $this->dropColumn($this->tableName1, 'translate');
        $this->dropForeignKey('fk-links-id-translate', $this->tableName2);
        $this->dropForeignKey('fk-book-id-translate', $this->tableName2);
        $this->dropTable($this->tableName2);
    }

}


