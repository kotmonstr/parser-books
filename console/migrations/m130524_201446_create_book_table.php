<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201446_create_book_table extends Migration
{

    protected $tableName = '{{%book}}';

    public function up()
    {

        $this->createTable($this->tableName, [
            'id' => Schema::TYPE_PK,
            'link_id' => Schema::TYPE_INTEGER,
            'row_html' => $this->text()->notNull(),
            'title' => $this->string(255),
            'descr' => $this->string(255),
            'seller' => $this->string(255),
            'condition' => $this->string(255),
            'image' => $this->string(255),
            'price' =>  $this->text(),
            'year' => $this->text(),
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        // indexes
        $this->createIndex('book-link-id', $this->tableName, 'link_id');

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-links-id',//name
            'book', //current table
            'link_id',//current my field
            'links', //
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-links-id', $this->tableName);
        $this->dropTable($this->tableName);

    }

}


