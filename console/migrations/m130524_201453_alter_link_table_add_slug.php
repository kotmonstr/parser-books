<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201454_add_index_to_link_table_to_url_table extends Migration
{

    protected $tableName = '{{%links}}';
    protected $tableName2 = '{{%url}}';

    public function up()
    {
        $this->addColumn($this->tableName,'slug', $this->text());
        $this->addColumn($this->tableName2,'slug', $this->text());
    }

    public function down()
    {
        $this->dropColumn($this->tableName,'slug', $this->text());
        $this->dropColumn($this->tableName2,'slug', $this->text());
    }

}


