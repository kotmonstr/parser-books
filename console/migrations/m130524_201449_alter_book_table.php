<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201449_alter_book_table extends Migration
{

    protected $tableName = '{{%book}}';

    public function up()
    {
        $this->addColumn( $this->tableName, 'xml_file_name', $this->string(255)->notNull()) ;
    }

    public function down()
    {
        $this->dropColumn($this->tableName, 'xml_file_name');
    }

}


