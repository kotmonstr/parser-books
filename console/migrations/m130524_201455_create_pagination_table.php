<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201455_create_pagination_table extends Migration
{

    protected $tableName = '{{%pagination}}';


    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => Schema::TYPE_PK,
            'url' => Schema::TYPE_STRING,
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}


