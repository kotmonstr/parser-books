<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201447_alter_url_table extends Migration
{

    protected $tableName = '{{%url}}';

    public function up()
    {
        $this->addColumn( $this->tableName, 'xml_file_name', $this->string(255)->notNull()) ;
    }

    public function down()
    {
        $this->dropColumn($this->tableName, 'xml_file_name');
    }

}


