<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201450_alter_book_table_addcity extends Migration
{

    protected $tableName = '{{%book}}';

    public function up()
    {
        $this->addColumn( $this->tableName, 'seller_city', $this->string(255)->notNull()) ;
        $this->alterColumn($this->tableName,'image', $this->text());
        $this->addColumn($this->tableName,'addition_descr', $this->text());
        $this->addColumn($this->tableName,'izdatel_city', $this->text());
    }

    public function down()
    {

    }

}


