<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201454_add_index_to_link_table_to_url_table extends Migration
{

    protected $tableName1 = '{{%links}}';
    protected $tableName2 = '{{%url}}';

    public function up()
    {
        $this->alterColumn($this->tableName1,'slug', $this->string(32));
        $this->alterColumn($this->tableName2,'slug',$this->string(32));

        // indexes
        $this->createIndex('index-links-slug', $this->tableName1,'slug');
        $this->createIndex('index-url-slug', $this->tableName2,'slug');
    }

    public function down()
    {
        $this->dropIndex('index-links-slug', $this->tableName1);
        $this->dropColumn('index-url-slug', $this->tableName2);
    }

}


