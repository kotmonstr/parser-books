<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201445_create_links_table extends Migration
{

    protected $tableName = '{{%links}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') { $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'; }

        $this->createTable($this->tableName, [
            'id' => Schema::TYPE_PK,
            'href' => $this->string('250')->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // indexes
        $this->createIndex('my_href-x', $this->tableName, 'href');


    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}


