<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201443_create_url_table extends Migration
{
    /** @inheritdoc */
    protected $tableName = '{{%url}}';

    public function up()
    {
        $this->createTable('url', [
            'id' => Schema::TYPE_PK,
            'url' => Schema::TYPE_STRING,
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}


