<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201452_alter_book_and_translate_table extends Migration
{

    protected $tableName1 = '{{%book}}';
    protected $tableName2 = '{{%book_translate}}';

    public function up()
    {
        $this->alterColumn($this->tableName1,'descr', $this->text());
        $this->alterColumn($this->tableName2,'descr', $this->text());

    }

    public function down()
    {

    }

}


