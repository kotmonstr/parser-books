<?php
namespace console\controllers;

class TranslateController extends \yii\console\Controller
{
    //const KEY = "trnsl.1.1.20190304T084626Z.19ee579a013d50bf.3a7a1fe8b4e6b61fa110de2d775aaa1932872e98"; // kotkrim
    const KEY = "trnsl.1.1.20190304T092450Z.dfe2c2c37b0cb874.6f797bed6a5e208205a59f1adcca18cfd40ee43c"; // testolegkot
    const LANGUAGE = "ru-en";

    public function actionTest()
    {
        $text = "Привет мир! Как дела Антон?";
        $url = "https://translate.yandex.net/api/v1.5/tr/translate?key=" . self::KEY . "&lang=" . self::LANGUAGE . "&text=" . $text;
        $result = simplexml_load_file($url);
        echo "Результат: ".$result->text;
    }

    public function actionTest2()
    {
        //http://chuvyr.ru/2014/01/gtranslate.html

        $str = "Привет мир! Как дела Антон? Думаю что погода будет солнечной !!!";

        $lang_from = "ru";
        $lang_to = "en";

        $query_data = array(
            'client' => 'x',
            'q' => $str,
            'sl' => $lang_from,
            'tl' => $lang_to
        );
        $filename = 'http://translate.google.ru/translate_a/t';
        $options = array(
            'http' => array(
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.0; rv:26.0) Gecko/20100101 Firefox/26.0',
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($query_data)
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($filename, false, $context);
        vd2( json_decode($response));




    }

}