<?php
namespace console\controllers;

use console\models\Book;
use console\models\Links;
use console\models\Url;
use Yii;
use Sunra\PhpSimple\HtmlDomParser;
use console\models\simple_html_dom;


class ParserController extends \yii\console\Controller
{
    //public $XML = 'sitemap_alib_ru.xml';
    const XML = 'tsiteau1.xml';
    const PROXY_SOURCE = 'https://free-proxy-list.net/anonymous-proxy.html';


    function init()
    {
        ini_set('default_charset', 'utf-8');
        mb_internal_encoding("UTF-8");
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '256M');
    }


    /**
     * STEP #1
     */
    public function actionGetUrl($filename){

        if(!$filename){
            echo "Забыли ввести имя файла! Например tsiteau1.xml";die();
        }

        $arrUrlList = [];
        $i = 0;

        // get all pages from xml
        $file = simplexml_load_file(Yii::getAlias('@dist/xml/'. $filename));
        foreach ($file as $block) {
            $i++;
            $arrUrlList[$i] = (string)$block->loc;
        }

        // put urls to table
        foreach ($arrUrlList as $url) {
            if (Url::checkUniqUrl(md5($url)) == true) {
                Url::AddUrlToTable($url, $filename);
                echo $url. PHP_EOL;
            }else{
                echo "Уже есть такао url в таблице Url". PHP_EOL;
            }

        }
    }

    /**
     * STEP #2
     */
    public function actionParse($shift=null, $all=false){

        $modelUrlsCount = Url::find()->count();
        if($all){
            $modelUrlsTotal = Url::find()->count();
        }else{
            $modelUrlsTotal = Url::find()->active()->count();
        }

        $modelUrls = Url::find()->select(['url','id','xml_file_name'])->asArray()->active()->offset($shift)->all();

        echo "Запуск парсера для таблицы Links...".PHP_EOL;
        echo "Осталось не обработанных cтраниц ". $modelUrlsTotal . PHP_EOL;

        foreach ($modelUrls as $url){

            echo 'Страница '. $url['id'] . ' из '.$modelUrlsCount . PHP_EOL;
            $dom = $this->Parser( $url['url']);
            $dom = HtmlDomParser::str_get_html( $dom );
            if(!empty($dom)){
                foreach($dom->find('a') as $el){

                    if($el->plaintext == 'Купить' ){
                        if(Links::checkUniqHref(md5($el->href)) == true){
                            Links::addToTable($el->href,$url['xml_file_name']);
                            echo $el->href. PHP_EOL;
                        }else{
                            echo "Уже есть такая книга в таблице Links". PHP_EOL;
                        }
                    }

                }
            }
            Url::resolveUrl( $url['id']);
        }
    }

    /**
     * STEP #3 geting row html from links into books table
     */
    public function actionAddBook($shift=null){
        echo "Запуск парсера для таблицы Book...со сдвигом = ".$shift.PHP_EOL;
        $modelLinks = Links::find()->select(['href','id', 'xml_file_name'])->asArray()->offset($shift)->active()->all();

        if(!empty($modelLinks)){
            foreach($modelLinks as $link){

                $dom = $this->Parser($link['href']);
                $dom = HtmlDomParser::str_get_html( $dom );

                if(isset($dom) && !empty($dom)){

                    $text = $dom->find('table',3);
                    if($text){
                        $text = $text->find('td',0);

                        //clear tags
                        $text = str_replace('<h3>Вы выбрали купить:</h3>','',$text);
                        $text = str_replace('<td>','',$text);
                        $text = str_replace('</td>','',$text);

                        if(Book::checkUniqContent($link['id']) && !Book::checkIsItAdvert($text) && !Book::checkIsItMetrika($text)){
                            Book::addRowHtml($text,$link['id'],$link['xml_file_name']);
                            Links::resolveLink($link['id']);
                            echo 'Добавленна новая книга'. PHP_EOL;
                        }else{
                            echo "Уже есть такая книга в таблице Books". PHP_EOL;
                            Links::resolveLink($link['id']);
                        }

                    }else{
                        echo 'Страница временно не доступна';
                        echo '---------';
                    }

                }else{
                    echo 'Пустая страница'.PHP_EOL;
                }
            }
        }

    }

    /*
     * Основной метод парсера
     */
    public function Parser($Url){
        $Url = trim(str_replace("www.","",$Url));
        echo "#Parsing..". $Url. PHP_EOL;

        $arrAgent = file(Yii::getAlias('@console/models/useragent_list.txt'));
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console/models/proxy_list.txt'));
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":",trim($arrProxi[$ProxiKey]));

        if(!filter_var($Url, FILTER_VALIDATE_URL))
               {
                       return "error";
               }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$Url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, trim($proxiMOD[0]));
        curl_setopt($ch, CURLOPT_PROXYPORT, trim($proxiMOD[1]));

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
    }

     /**
     * пересоздаю файл с новыми проксиками
     */
    public function actionEditProxy()
    {
        echo "#FindNewProxi..",PHP_EOL;
        $listProxi = file_get_contents('http://randgate.wecandevelopit.com/1000/100');
        $arrListProxi = json_decode($listProxi);

        foreach ($arrListProxi as $proxi) {
            $arrProxi[]=$proxi->ip . ':' . $proxi->port;
        }

        $i=0;
        $arrProxiString='';
        $file = Yii::getAlias('@console/models/proxy_list.txt');
        file_put_contents($file, '');
        foreach ($arrProxi as $proxi) {

            $url = 'http://google.com';
            $proxiMOD = explode(":",trim($proxi));
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
            curl_setopt($ch, CURLOPT_PROXY, $proxiMOD[0]);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxiMOD[1]);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $output = curl_exec($ch);

            if ($output === FALSE) {
                echo $proxiMOD[0].':'.$proxiMOD[1].' -'. curl_error($ch),PHP_EOL;
            }else{
                echo $proxiMOD[0].':'.$proxiMOD[1]. ' -ok',PHP_EOL;
                $i++;
                $arrRusult[$i]= $proxi;
                $arrProxiString .= trim($proxiMOD[0]) . ':' . trim($proxiMOD[1]) . "\n";
                file_put_contents($file, $arrProxiString,FILE_APPEND);

            }
            curl_close($ch);
        }
        echo "Was founded: ".$i. ' proxis.',PHP_EOL;
    }

    public function actionGetXml(){

        for ($i = 1; $i <= 70; $i++) {
           shell_exec('wget -P /home/kot/www/parser.loc/dist/xml/ http://www.alib.ru/tsiteau'.$i.'.xml');
        }

    }

    public function actionGetNewProxy()
    {
        $arr = [];
        $file = Yii::getAlias('@console/models/proxy_list.txt');
        $dom = file_get_contents(self::PROXY_SOURCE);
        $dom = HtmlDomParser::str_get_html($dom);

        if (isset($dom) && !empty($dom)) {
            $table = $dom->find('table',0);
                foreach ($table->find('tr') as $tr){
                    $td = strip_tags($tr->find('td',0));
                    $td1 = strip_tags($tr->find('td',1));
                    if($this->checkProxy($td .':'. $td1) ){
                        $arr[]= $td .':'. $td1;
                    }
                }
                file_put_contents($file, '');
                foreach ($arr as $item){
                    file_put_contents($file, $item . "\n",FILE_APPEND);
                }
                echo "Прокси успешно обновлены.".PHP_EOL;
        }
    }
    public function checkProxy($proxy)
    {
        $url = 'http://google.com';
        $proxiMOD = explode(":", trim($proxy));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
        curl_setopt($ch, CURLOPT_PROXY, $proxiMOD[0]);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxiMOD[1]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $output = curl_exec($ch);

        if ($output === FALSE ||  $proxiMOD[0] . ':' . $proxiMOD[1] == ':') {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -' . curl_error($ch), PHP_EOL;
            return false;
        } else {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -ok', PHP_EOL;
            return true;
        }
    }

}