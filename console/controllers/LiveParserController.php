<?php
namespace console\controllers;

use console\models\Book;
use console\models\Links;
use console\models\Pagination;
use console\models\Url;
use Yii;
use Sunra\PhpSimple\HtmlDomParser;
use console\models\simple_html_dom;


class LiveParserController extends \yii\console\Controller
{
    const SITE_URL = 'http://alib.ru/kat.phtml';

    /* Запуск скриптов
     * php yii live-parser/get-category
     * php yii live-parser/collect-links
     * php yii live-parser/collect-from-pagination
     */

    function init()
    {
        ini_set('default_charset', 'utf-8');
        mb_internal_encoding("UTF-8");
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '256M');
    }
    public function actionGetCategory()
    {
        $file = Yii::getAlias('@console/models/categories.txt');
        $arrResult=[];
        $dom = $this->Parser(self::SITE_URL);
        $dom2 = HtmlDomParser::str_get_html( $dom );

        if(!empty($dom2)){ // в категории
            foreach($dom2->find('a') as $el){
                $regexp = '~phtml$~';
                $match = [];
                if (preg_match($regexp, trim($el->href), $match)) {
                    $arrResult[]= $el->href;
                echo "Нашел ". $el->href. PHP_EOL;
                } else {
                    echo "Не нашел ". $el->href. PHP_EOL;
                }
            }
        }

        file_put_contents($file,"");
        foreach ($arrResult as $item){
            file_put_contents($file, $item . "\n",FILE_APPEND);
        }
       echo "Сбор категорий окончен".PHP_EOL;
    }
    public function actionCollectLinks(){
        $arrCategory = file(Yii::getAlias('@console/models/categories.txt'));
        $arrResult=[];
        $arrResult2=[];

        foreach ($arrCategory as $categoryLink){
            $dom = $this->Parser($categoryLink);
            $dom2 = HtmlDomParser::str_get_html( $dom );
            if(!empty($dom2)){
                foreach($dom2->find('a') as $el){

                    if($el->plaintext == 'Купить' ){
                        if(Links::checkUniqHref(md5($el->href)) == true){
                            Links::addToTable($el->href,'live');
                            echo $el->href. PHP_EOL;
                        }else{
                            echo "Уже есть такая книга в таблице Links". PHP_EOL;
                        }
                    }
                    // сбор пагинации
                    $regexp = '~http://www.alib.ru/razdel.php4?~';
                    $match = [];
                    if (preg_match($regexp, trim($el->href), $match)) {
                        Pagination::addToTable($el->href);
                        echo "Нашел ". $el->href. PHP_EOL;
                    } else {
                        echo "Не нашел ". $el->href. PHP_EOL;
                    }
                }
            }

                // удалить строку
                $file = file(Yii::getAlias('@console/models/categories.txt'));
                $fp = fopen(Yii::getAlias('@console/models/categories.txt'),"w");
                unset($file[0]);
                fputs($fp,implode("",$file));
                fclose($fp);
                echo "Строка удаленна". PHP_EOL;
            }
    }
    public function actionCollectFromPagination(){

        $model = Pagination::find()->select(['id','url'])->asArray()->where(['status'=> Pagination::NONE_RESOLVED])->all();

        foreach ($model as $pagerPage){

            $url =  preg_replace("~&amp;~", "&", $pagerPage['url']);
            //echo($url);exit();

            //$url = html_entity_decode($pagerPage['url']);

            //vd( $pagerPage['id'], false);
            //vd( $pagerPage['url'] .PHP_EOL. $url);

            $dom = $this->Parser($url);
            $dom2 = HtmlDomParser::str_get_html( $dom );
            if(!empty($dom2)){
                foreach($dom2->find('a') as $el){

                    if($el->plaintext == 'Купить' ){
                        if(Links::checkUniqHref(md5($el->href)) == true){
                            Links::addToTable($el->href,'live');
                            echo $el->href. PHP_EOL;
                        }else{
                            echo "Уже есть такая книга в таблице Links". PHP_EOL;
                        }
                    }else{
                        echo "Не нашел ссылок на странице ". $el->href . PHP_EOL;
                    }

                }
                Pagination::resolveLink($pagerPage['id']);

            }else{
                echo "Не смог достучаться". PHP_EOL;
            }


        }

    }

    /*
 * Основной метод парсера
 */
    public function Parser($Url){
        $Url = trim(str_replace("www.","",$Url));
        echo "#Parsing..". $Url. PHP_EOL;

        $arrAgent = file(Yii::getAlias('@console/models/useragent_list.txt'));
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console/models/proxy_list.txt'));
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":",trim($arrProxi[$ProxiKey]));

        if(!filter_var($Url, FILTER_VALIDATE_URL))
        {
            return "error";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$Url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, trim($proxiMOD[0]));
        curl_setopt($ch, CURLOPT_PROXYPORT, trim($proxiMOD[1]));

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
    }

}