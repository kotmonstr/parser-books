<?php
namespace console\controllers;

use console\models\BookTranslate;
use console\models\Links;
use console\models\Url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Yii;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sunra\PhpSimple\HtmlDomParser;
use console\models\Book;
use yii\db\Query;
use console\models\City;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class HelpsController extends \yii\console\Controller
{
    public function actionUpdateLink(){

        $modelLinks = Links::find()->all();
        foreach ($modelLinks as $link){
            echo $link->href.PHP_EOL;
            $model = Links::find()->where(['id'=> $link->id])->one();
            $model->slug = md5($model->href);
            $model->updateAttributes(['slug']);
        }

    }

    public function actionUpdateUrl(){

        $modelUrl = Url::find()->all();
        foreach ($modelUrl as $url){
            echo $url->url.PHP_EOL;
            $model = Url::find()->where(['id'=> $url->id])->one();
            $model->slug = md5($model->url);
            $model->updateAttributes(['slug']);
        }

    }

    public function actionCreateExelFile($limit = null)
    {

        $i = 2;
        $file = Yii::getAlias('@dist/clear.xlsx');
        $file2 = Yii::getAlias('@dist/result.xlsx');

        $spreadsheet = IOFactory::load($file);
        $sheet = $spreadsheet->getActiveSheet();

        $query = new Query;
        $query->select(['*',
            'book_translate.price As PRICE',
            'book_translate.id As ID',
            'book_translate.descr As descr-english',
            'book_translate.izdatel_city As izdatel_city-english',
            'book_translate.title As title-english',
            'book_translate.year As year-english',
            'book.descr As title-ru',
        ])
            ->from('book_translate')
            ->leftJoin('book', 'book_translate.book_id = book.id')
            ->orderBy('book_translate.id DESC')
            ->limit($limit);

        $command = $query->createCommand();
        $data = $command->queryAll();

        if ($data) {
            foreach ($data as $row) {

                $i++;
                echo $i.PHP_EOL;

                    $sheet->setCellValue('A' . $i, 'alb' . $row['ID']);
                    $sheet->setCellValue('B' . $i, 'Книги');
                    $sheet->setCellValue('C' . $i, $row['title']);
                    $sheet->setCellValue('D' . $i, $row['year']);
                    $sheet->setCellValue('F' . $i, $row['izdatel_city']);
                    $sheet->setCellValue('G' . $i, $row['descr']);
                    $sheet->setCellValue('H' . $i, $row['PRICE']);
                    $sheet->setCellValue('I' . $i, 'Alib');
                    $sheet->setCellValue('J' . $i, 'Alib');
                    $sheet->setCellValue('L' . $i, $this->getDollarPrice((Int)$row['PRICE']));
                    $sheet->setCellValue('N' . $i, $row['title-english']);
                    $sheet->setCellValue('O' . $i, $this->transliteration($row['title']));
                    $sheet->setCellValue('P' . $i, $row['izdatel_city-english']);
                    $sheet->setCellValue('R' . $i, $row['descr-english']);
                    $sheet->setCellValue('Y' . $i, $this->clearString( $row['title-english'] . "/" . $this->transliteration($row['title']) . '. ' . $row['izdatel_city-english'] . ', ' . $row['year']));
                    $sheet->setCellValue('Z' . $i, $this->clearString("Please feel free to request a detailed description. Short description: " . $row['izdatel_city-english'] . ", " . $row['year'] . ". SKUalb" . $row['ID']));
                    $sheet->setCellValue('AA' . $i, $this->getDollarPrice((Int)$row['price']));
                    $sheet->setCellValue('AB' . $i, $this->clearString($this->getSCV($row['title-english'] . ',' . $this->transliteration($row['title']) . ',' . $row['izdatel_city-english'] . ',' . $row['year']))); // перечислить все это через запятые
                    $sheet->setCellValue('AC' . $i, 'Нет');
                    $sheet->setCellValue('AD' . $i, $this->clearString($row['title-english'] . "/" . $this->transliteration($row['title']) . '. ' . $row['izdatel_city-english'] . ', ' . $row['year']));
                    $sheet->setCellValue('AE' . $i, $this->clearString("Please feel free to request a detailed description. Short description: " . $row['izdatel_city-english'] . ", " . $row['year'] . ". SKUalb" . $row['ID']));
                    $sheet->setCellValue('AF' . $i, $this->getDollarPrice((Int)$row['PRICE']));
                    $sheet->setCellValue('AG' . $i, $this->clearString( $this->getSCV($row['title-english'] . ',' . $this->transliteration($row['title']) . ',' . $row['izdatel_city-english'] . ',' . $row['year']))); // перечислить все это через запятые
                    $sheet->setCellValue('AH' . $i, 'Нет');
                    $sheet->setCellValue('BM' . $i, 'Перевыставление c Alib');

                }
            $writer = new Xlsx($spreadsheet);
            $writer->setPreCalculateFormulas(false);
            $writer->setOffice2003Compatibility(true);
            $writer->save($file2);

        }
    }

    public function actionMultyTest($shift=null,$limit=20){
        $i =0;
        $modelUrls = Url::find()->select(['url','id','xml_file_name'])->asArray()->active()->offset($shift)->limit($limit)->all();
        $multiCurl = [];
        $result = [];
        $mh = curl_multi_init();
        $arrAgent = file(Yii::getAlias('@console/models/useragent_list.txt'));
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console/models/proxy_list.txt'));
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":",trim($arrProxi[$ProxiKey]));

        if(!empty($modelUrls)){
            foreach ($modelUrls as $url) {
                $i++;
                $arrTemp[$i]['url']=$url['url'];
                $arrTemp[$i]['xml_file_name']=$url['xml_file_name'];
                $arrTemp[$i]['id']=$url['id'];
                $fetchURL = $url['url'];
                $multiCurl[$i] = curl_init();
                curl_setopt($multiCurl[$i], CURLOPT_URL,$fetchURL);
                curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
                curl_setopt($multiCurl[$i], CURLOPT_USERAGENT, $agent);
                curl_setopt($multiCurl[$i], CURLOPT_PROXY, trim($proxiMOD[0]));
                curl_setopt($multiCurl[$i], CURLOPT_PROXYPORT, trim($proxiMOD[1]));
                curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$i], CURLOPT_CUSTOMREQUEST,'GET');
                curl_setopt($multiCurl[$i], CURLOPT_CONNECTTIMEOUT, 60);
                curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT, 10);

                curl_multi_add_handle($mh, $multiCurl[$i]);
            }
            $index=null;
            do {
                curl_multi_exec($mh,$index);
            } while($index > 0);

            foreach($multiCurl as $k => $ch) {
                $result[$k]['html'] = curl_multi_getcontent($ch);
                $result[$k]['add'] = $arrTemp[$k];
                curl_multi_remove_handle($mh, $ch);
            }

            curl_multi_close($mh);

            foreach ($result as $item){
                $dom = HtmlDomParser::str_get_html( $item['html'] );
                if(!empty($dom)){
                    foreach($dom->find('a') as $el){

                        if($el->plaintext == 'Купить' ){
                            if(Links::checkUniqHref(md5($el->href)) == true){
                                Links::addToTable($el->href,$item['add']['xml_file_name']);
                                echo $el->href. PHP_EOL;
                            }else{
                                echo "Уже есть такая книга в таблице Links". PHP_EOL;
                            }
                        }
                    }
                }else{
                    echo"не смог достучаться к старнице: ".$item['add']['url']. PHP_EOL;
                }
                Url::resolveUrl( $item['add']['id']);
            }
        }else{
            echo"нет больше страниц". PHP_EOL;
        }

    }

    public function actionMultyAddBook($shift=null,$limit=30){

        echo "Запуск парсера для таблицы Book...со сдвигом = ".$shift.PHP_EOL;
        $modelLinks = Links::find()->select(['href','id', 'xml_file_name'])->asArray()->offset(5000)->active()->limit($limit)->all();
        $i =0;
        $multiCurl = [];
        $result = [];
        $mh = curl_multi_init();
        $arrAgent = file(Yii::getAlias('@console/models/useragent_list.txt'));
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console/models/proxy_list.txt'));
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":",trim($arrProxi[$ProxiKey]));

        if(!empty($modelLinks)){

            foreach ($modelLinks as $url) {
                $i++;

                echo $url['id'].PHP_EOL;
                //$arrTemp[$i]['url']=$url['url'];
                $arrTemp[$i]['xml_file_name']=$url['xml_file_name'];
                $arrTemp[$i]['id']=$url['id'];
                $fetchURL = $url['href'];
                $multiCurl[$i] = curl_init();
                curl_setopt($multiCurl[$i], CURLOPT_URL,$fetchURL);
                curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
                curl_setopt($multiCurl[$i], CURLOPT_USERAGENT, $agent);
                curl_setopt($multiCurl[$i], CURLOPT_PROXY, trim($proxiMOD[0]));
                curl_setopt($multiCurl[$i], CURLOPT_PROXYPORT, trim($proxiMOD[1]));
                curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($multiCurl[$i], CURLOPT_CUSTOMREQUEST,'GET');
                curl_setopt($multiCurl[$i], CURLOPT_CONNECTTIMEOUT, 60);
                curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT, 10);
                curl_multi_add_handle($mh, $multiCurl[$i]);
            }
            //vd($arrTemp);

            $index=null;
            do {
                curl_multi_exec($mh,$index);// warning!!!!
            } while($index > 0);

            foreach($multiCurl as $k => $ch) {
                $result[$k]['html'] = curl_multi_getcontent($ch);
                $result[$k]['add'] = $arrTemp[$k];
                curl_multi_remove_handle($mh, $ch);
            }

            curl_multi_close($mh);



            foreach ($result as $item){
                $dom = HtmlDomParser::str_get_html( $item['html'] );
                if(!empty($dom)){

                    $text = $dom->find('table',3);
                    if($text){
                        $text = $text->find('td',0);

                        //clear tags
                        $text = str_replace('<h3>Вы выбрали купить:</h3>','',$text);
                        $text = str_replace('<td>','',$text);
                        $text = str_replace('</td>','',$text);

                        if(Book::checkUniqContent($item['add']['id']) && !Book::checkIsItAdvert($text) && !Book::checkIsItMetrika($text)){
                            Book::addRowHtml($text,$item['add']['id'],$item['add']['xml_file_name']);
                            Links::resolveLink($item['add']['id']);
                            echo 'Добавленна новая книга'. PHP_EOL;
                        }else{
                            echo "Уже есть такая книга в таблице Books". PHP_EOL;
                            Links::resolveLink($item['add']['id']);
                        }

                    }else{
                        echo 'Страница временно не доступна';
                        echo '---------';
                    }

                }else{
                    echo 'Пустая страница'.PHP_EOL;
                }
            }
        }


    }

    private function getDollarPrice($price){
        //Столбец L т.е. цена в долларах рассчитывается по формуле - цена резерв умноженная на 2/1,5/1,3 (этот коэффициент меняется в зависимости от резервной цены, для цен <65000 руб используется 2, от 65000 до 650000 используется 1,5 и всё, что выше 650000 используется 1,3) + $100. результат этой операции округляется вверх до двух разрядов (т.е. всё что меньше 100, становится 100, от 100 до 200 становится 200 и т.д.) и из этого вычитается 1.
        //Таким образом минимальная цена будет $199, а дальше варируется в зависимости от исходной цены.
        switch ($price) {
            case $price <= 3000: // для цен дешевле 3000р
                $result = 199;
                break;
            case $price <= 130000: // для цен дешевле 2000$
                $result = $this->SetPrice((2 * $price)/ 65 );
                break;
            case $price >= 130000 && $price < 650000: //для цен  2000-10000 долл.
                $result = $this->SetPrice((1.5 * $price)/ 65 );
                break;
            case  $price >= 650000: //для цен 10001 долл. и дороже
                $result = $this->SetPrice((1.3 * $price)/ 65 );
                break;
        }
        return $result;

    }

    /*
     * Final
     */
    public function actionParseAndTranslate($limit=null)
    {
        $modelBook = Book::find()->select('id')->where(['translate'=> Book::NON_TRANSALATED])->asArray()->limit($limit)->all();

        foreach ($modelBook as $book){
            echo "Обрабатываю книгу с id= ".$book['id'].PHP_EOL;

            $model = Book::find()->where(['id'=> $book['id']])->one();
            $source = $model->row_html; // источник
            $model->title = $this->getTitle($source);
            $model->price = $this->getPrice($source);
            $model->year = $this->getYear($source);
            $model->condition = $this->getCondition($source);
            $model->seller = $this->getSeller($source);
            $model->seller_city = $this->getSellerCity($source);
            $model->descr = $this->getDescription($source);
            $model->image = serialize(json_encode($this->getImage($source)));
            $model->addition_descr =$this->getAdditionDescr($source);
            $model->izdatel_city =$this->getIzdatelCity($source);
            $model->translate = Book::NON_TRANSALATED;
            $model->updateAttributes(['title','price','year','condition','seller','seller_city','descr','image','addition_descr','translate','izdatel_city']);

            if( $model->translate == Book::NON_TRANSALATED){
                $source = $model->row_html; // источник
                $model->title = $this->getTitle($source);
                $model->price = $this->getPrice($source);
                $model->year = $this->getYear($source);
                $model->condition = $this->getCondition($source);
                $model->seller = $this->getSeller($source);
                $model->seller_city = $this->getSellerCity($source);
                $model->descr = $this->getDescription($source);
                $model->image = serialize(json_encode($this->getImage($source)));
                $model->addition_descr =$this->getAdditionDescr($source);
                $model->updateAttributes(['title','price','year','condition','seller','seller_city','descr','image','addition_descr']);

                $modelTranslate = new BookTranslate();
                $modelTranslate->link_id = $model->link_id;
                $modelTranslate->book_id = $model->id;
                $modelTranslate->row_html =  Html::decode($this->T($model->row_html));
                $modelTranslate->title =  Html::decode($this->T($model->title));
                $modelTranslate->descr =  Html::decode($this->T($model->descr));
                $modelTranslate->seller =  $model->seller;
                $modelTranslate->condition =  Html::decode($this->T($model->condition));
                $modelTranslate->image =  $model->image;
                $modelTranslate->price =  $model->price;
                $modelTranslate->year =  $model->year;
                $modelTranslate->translate = 1;
                $modelTranslate->seller_city =  $model->seller_city ?  Html::decode($this->T($model->seller_city)) : null;
                $modelTranslate->addition_descr =  $model->addition_descr ?  str_replace("</ p>","</p>",Html::decode($this->T($model->addition_descr))) : null;
                $modelTranslate->izdatel_city =  $model->izdatel_city ?   Html::decode($this->T($model->izdatel_city)) : null;
                $modelTranslate->xml_file_name = $model->xml_file_name;

                if( $modelTranslate->validate() &&  $modelTranslate->save() ){
                    $model->translate = Book::TRANSLATED;
                    $model->translate_id = $modelTranslate->id;
                    $model->updateAttributes(['translate','translate_id']);

                }else{
                    vd($modelTranslate->getErrors());
                    Yii::$app->session->setFlash('error', 'ERROR FOR TRANSLATE.');

                }
            }



        }
    }

    private function getTitle($source){

        $regexp = '~<b>.+\</b>~';

        $match = [];
        if (preg_match($regexp, $source, $match)) {
            //$x= "+ Найдено слово '{$match[0]}'\n";
            //vd($match,false);
            $x = $match[0];
        } else {
            $x= "";
        }

        return trim(strip_tags($x));
    }
    private function getPrice($source){

        $regexp = '~Цена:.+?руб.~m';

        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }

        return trim(str_replace("руб.","",str_replace("Цена:","",$x)));
    }
    private function getYear($source){
        $regexp = '~[0-9]{4}г\.~';
        $regexp2 = '~[0-9]{4}\s+г\.~';
        $regexp3 = '~[0-9]{4}\(~';
        $match = [];
        if (preg_match($regexp, $source, $match) || preg_match($regexp2, $source, $match) || preg_match($regexp3, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return trim(str_replace("(","",str_replace("г.","",$x)));
    }
    private function getCondition($source){
        $regexp = '~Состояние:.+?\<~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace("<","",str_replace("Состояние:","",$x)));
    }
    private function getSeller($source){

        $regexp = '~BS.+?<~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }

        return  urldecode(trim(str_replace("BS -","",str_replace("<","",preg_replace("~BS.+?>~","",$x)))));
    }
    private function getSellerCity($source){
        $regexp = '~a\>,.+?\)~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace(".)","",str_replace("a>,","",$x)));
    }
    private function getDescription($source){
        $regexp = '~/b>.+?[0-9]{2,}.+?с\.~';
        $regexp2 = '~/b>.+?<br~';
        $match = [];
        if (preg_match($regexp, $source, $match) || preg_match($regexp2, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  trim(str_replace("<br","",str_replace("/b>","",$x)));
    }
    private function getImage($source){
        $i=0;
        $result= [];
        $source = str_replace("amp;","",$source);
        $regexp = '~http:\/\/www.alib.ru\/foto.php4\?.+?\"~';
        $match = [];
        if (preg_match_all($regexp, $source, $match)) {
            $x = $match;
        } else {
            $x=[];
        }
        if(!empty($x[0])){
            foreach ($x[0] as $row => $item){
                $i++;
                $result[$i] = trim(str_replace("\"","",$item));
            }
        }


        return  $result;
    }
    private function getAdditionDescr($source){

        $regexp = '~\sс\..+?Состояние~';
        $regexp2 = '~[0-9]{2,}с\..+?Состояние~';
        $regexp3 = '~[0-9]{2,}с.+?Состояние~';
        $regexp4 = '~[0-9]{2,}.c\..+?Состояние~';

        $match = [];
        if ( preg_match($regexp, $source, $match ) || preg_match($regexp2, $source, $match) ||  preg_match($regexp3, $source, $match) || preg_match($regexp4, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        return  '<p>'.trim(preg_replace("~^\.~","",preg_replace("~с\.~","",preg_replace("~[0-9]{1,}.c\.~","",preg_replace("~[0-9]{1,}.c\.~","",preg_replace("~[0-9]{1,}с~","",str_replace("Состояние","",$x))))))).'</p>';
    }
    private function getIzdatelCity($source){


        $regexp = '~/b>.+?<br~';
        $match = [];
        if (preg_match($regexp, $source, $match)) {
            $x = $match[0];
        } else {
            $x= "";
        }
        $source = trim($x);


        //************************ cityes ****************************************************************
        $arr = file(Yii::getAlias('@console/models/city.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($arr as $row){
            $arrCity1[]['city'] = $row;
        }
        $arrCity2 = City::getlistCountriesByRussian();
        $arrayTotal= ArrayHelper::merge($arrCity1, $arrCity2);
        //обрезка фамилий с инициалами
        $regexpFIO = '~(\w+)\s[А-ЯA-Z]{1}\.\s[А-ЯA-Z]{1}\.\s~u';
        $source = preg_replace($regexpFIO,"",$source);

        $regexpFIO2 = '~\s[А-ЯA-Z]{3,}\s[А-ЯA-Z]{3}\.\s[А-ЯA-Z]{3}\.\s~u';
        $source = preg_replace($regexpFIO2,"",$source);

        foreach ($arrayTotal as $item){
            //vd('Ищу по городу: '.$item["city"].PHP_EOL,false);
            $regexp = '~\s'.$item["city"].'\s~';
            $regexp2 = '~\s'.$item["city"].',\s~';
            $regexp3 = '~\s'.$item["city"].':~';
            $regexp4 = '~\sг\.'.$item["city"].':~';
            $regexp5 = '~\s'.$item["city"].'\.\s~';

            $match = [];
            if (
                preg_match($regexp, $source, $match)
                || preg_match($regexp2, $source, $match)
                || preg_match($regexp3, $source, $match)
                || preg_match($regexp4, $source, $match)
                || preg_match($regexp5, $source, $match)
            ){

                $x = $match[0];

                // ичключения ************************************
                $item["city"] = $this->FixExseption($item["city"]);
                $x = trim($item["city"]);
                break;
            } else {
                $x= null;
            }
        }
        return  trim($x);
    }
    private function FixExseption($city){
        $result = $city;
        if($city == 'М'){$result = 'Москва'; }
        if($city == 'М.'){$result = 'Москва'; }
        if($city == 'Спб.'){$result = 'Санкт-Петербург'; }
        if($city == 'Л.'){$result = 'Ленинград'; }
        if($city == 'Лен'){$result = 'Ленинград'; }
        if($city == 'Ростов-н-Д'){$result = 'Ростов-на-Дону'; }
        if($city == 'Йорк'){$result = 'Нью-Йорк'; }
        if($city == 'СПб.'){$result = 'Санкт-Петербург'; }
        if($city == 'Спб'){$result = 'Санкт-Петербург'; }
        if($city == 'М.-Л.'){$result = 'Москва - Ленинград'; }

        return $result;
    }
    private function T($text){

        //http://chuvyr.ru/2014/01/gtranslate.html

        $lang_from = "ru";
        $lang_to = "en";

        $query_data = array(
            'client' => 'x',
            'q' => $text,
            'sl' => $lang_from,
            'tl' => $lang_to
        );
        $filename = 'http://translate.google.ru/translate_a/t';
        $options = array(
            'http' => array(
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.0; rv:26.0) Gecko/20100101 Firefox/26.0',
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($query_data)
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($filename, false, $context);
        return  json_decode($response);
    }

    private function transliteration($str)
    {
        // ГОСТ 7.79B
        $transliteration = array(
            'А' => 'A', 'а' => 'a',
            'Б' => 'B', 'б' => 'b',
            'В' => 'V', 'в' => 'v',
            'Г' => 'G', 'г' => 'g',
            'Д' => 'D', 'д' => 'd',
            'Е' => 'E', 'е' => 'e',
            'Ё' => 'Yo', 'ё' => 'yo',
            'Ж' => 'Zh', 'ж' => 'zh',
            'З' => 'Z', 'з' => 'z',
            'И' => 'I', 'и' => 'i',
            'Й' => 'J', 'й' => 'j',
            'К' => 'K', 'к' => 'k',
            'Л' => 'L', 'л' => 'l',
            'М' => 'M', 'м' => 'm',
            'Н' => "N", 'н' => 'n',
            'О' => 'O', 'о' => 'o',
            'П' => 'P', 'п' => 'p',
            'Р' => 'R', 'р' => 'r',
            'С' => 'S', 'с' => 's',
            'Т' => 'T', 'т' => 't',
            'У' => 'U', 'у' => 'u',
            'Ф' => 'F', 'ф' => 'f',
            'Х' => 'H', 'х' => 'h',
            'Ц' => 'Cz', 'ц' => 'cz',
            'Ч' => 'Ch', 'ч' => 'ch',
            'Ш' => 'Sh', 'ш' => 'sh',
            'Щ' => 'Shh', 'щ' => 'shh',
            'Ъ' => 'ʺ', 'ъ' => 'ʺ',
            'Ы' => 'Y`', 'ы' => 'y`',
            'Ь' => '', 'ь' => '',
            'Э' => 'E`', 'э' => 'e`',
            'Ю' => 'Yu', 'ю' => 'yu',
            'Я' => 'Ya', 'я' => 'ya',
            '№' => '#', 'Ӏ' => '‡',
            '’' => '`', 'ˮ' => '¨',
        );

        $str = strtr($str, $transliteration);
        $str = mb_strtolower($str, 'UTF-8');
       // $str = preg_replace('/[^0-9a-z\-]/', '', $str);
        //$str = preg_replace('|([-]+)|s', '-', $str);
        //$str = trim($str, '-');

        $str = preg_replace('~`~', '', $str);

        return $str;
    }

    private function getSCV($str){
        $str =  str_replace(".","",str_replace("\"","",str_replace(",,",",",str_replace(" ",",",$str))));
        return  trim(str_replace("\(","",str_replace("\)","",$str)));
    }

    private function SetPrice($price){
        return round(ceil($price/100)*100)+99;
    }

    private function clearString($str){

        $str = preg_replace("~, \.~", "",$str);
        $str = preg_replace("~\.\.~", "", $str);
        $str = preg_replace("~,\.~", "", $str);
        $str = preg_replace("~\.,~", "", $str);
        $str = preg_replace("~^,~", "", $str);
        $str = preg_replace("~Short description: ,~", "Short description:", $str);

        return  trim($str);
    }

}